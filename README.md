# Latihan Tugas Pemrograman

CSGE601021 Dasar-Dasar Pemrograman 2 @ Fakultas Ilmu Komputer Universitas Indonesia,
Semester Genap 2021/2022

***

Repositori ini merupakan repositori latihan yang digunakan sebagai pengantar tugas pemrograman. Silakan baca panduan [Getting Started with a Java Project][dokumen-getting-started] untuk memulai. Setelah mengikuti panduan tersebut, diharapkan mahasiswa menjadi familiar dengan *tools* yang digunakan pada tugas pemrograman.

[dokumen-getting-started]: https://docs.google.com/document/d/1oci5OI4Mxqp83Ph1-scHwl6l7OOU_co7ai0xPXJRhq0/edit?usp=sharing